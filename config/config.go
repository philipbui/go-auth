package config

import (
	"flag"
)

var (
	Key           string
	Port          string
	RedisServer   string
	RedisPassword string
)

func init() {
	flag.StringVar(&Key, "Key", "PhilipsKey", "Key for encoding and decoding JWTs")
	flag.StringVar(&Port, "Port", ":8079", "Port for Server")
	flag.StringVar(&RedisServer, "RedisServer", ":6379", "")
	flag.StringVar(&RedisPassword, "RedisPassword", "", "")
}

func init() {
	flag.Parse()
}
