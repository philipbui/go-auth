package route

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Based off https://blog.golang.org/error-handling-and-go
type HttpError struct {
	Error   error
	Message string
	Code    int
}
type Controller func(http.ResponseWriter, *http.Request, httprouter.Params) *HttpError

func ServeHTTP(fn Controller) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		if err := fn(w, r, ps); err != nil {
			http.Error(w, err.Message+": "+err.Error.Error(), err.Code)
		}
	}
}
