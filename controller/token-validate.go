package controller

import (
	"auth/route"
	"auth/service"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func ValidateToken(w http.ResponseWriter, r *http.Request, ps httprouter.Params) *route.HttpError {
	token, httpError := service.ParseToken(r)
	if httpError != nil {
		return httpError
	}
	conn := service.Pool.Get()
	defer conn.Close()
	if httpError := service.ValidateToken(conn, token.Claims["aud"], token.Claims["jti"]); httpError != nil {
		return httpError
	}
	fmt.Fprint(w, &createTokenRequest{token.Claims["aud"].(string)})
	return nil
}
