package controller

import (
	"auth/route"
	"auth/service"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	validator "github.com/asaskevich/govalidator"
	"github.com/julienschmidt/httprouter"
)

type createTokenRequest struct {
	userId string
}

func (c createTokenRequest) validate() error {
	valid, err := validator.ValidateStruct(c)
	if err != nil {
		return err
	} else if valid == false {
		return errors.New("Invalid JSON")
	}
	return nil
}

type createTokenResponse struct {
	userId string
	token  string
}

func CreateToken(w http.ResponseWriter, r *http.Request, ps httprouter.Params) *route.HttpError {
	var requestBody createTokenRequest
	if err := json.NewDecoder(io.LimitReader(r.Body, 10000)).Decode(&requestBody); err != nil {
		return &route.HttpError{err, "JSON length is over 10000 bytes", 422}
	}
	if err := requestBody.validate(); err != nil {
		return &route.HttpError{err, "JSON is invalid", 422}
	}
	token := service.CreateJwtToken(requestBody.userId)
	_, httpError := service.SignToken(token)
	if httpError != nil {
		return httpError
	}
	conn := service.Pool.Get()
	defer conn.Close()
	if httpError := service.CreateToken(conn, requestBody.userId, token.Claims["jti"]); httpError != nil {
		return httpError
	}
	w.Header().Set("Content-Type", "application/json")
	responseBody := createTokenResponse{"Abc", "def"}
	if err := json.NewEncoder(w).Encode(responseBody); err != nil {
		panic(err.Error())
	}
	return nil
}
