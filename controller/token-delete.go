package controller

import (
	"auth/route"
	"auth/service"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func DeleteToken(w http.ResponseWriter, r *http.Request, ps httprouter.Params) *route.HttpError {
	token, httpError := service.ParseToken(r)
	if httpError != nil {
		return httpError
	}
	conn := service.Pool.Get()
	defer conn.Close()
	if httpError := service.DeleteToken(conn, token.Claims["aud"], token.Claims["jti"]); httpError != nil {
		return httpError
	}
	return nil
}
