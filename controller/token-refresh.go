package controller

import (
	"auth/route"
	"auth/service"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func RefreshToken(w http.ResponseWriter, r *http.Request, ps httprouter.Params) *route.HttpError {
	token, httpError := service.ParseToken(r)
	if httpError != nil {
		return httpError
	}
	conn := service.Pool.Get()
	defer conn.Close()
	newToken := service.CreateJwtToken(token.Claims["aud"].(string))
	newTokenString, httpError := service.SignToken(newToken)
	if httpError != nil {
		return httpError
	}
	if httpError := service.CreateToken(conn, newToken.Claims["aud"], newToken.Claims["jti"]); httpError != nil {
		return httpError
	}
	if httpError := service.DeleteToken(conn, token.Claims["aud"], token.Claims["jti"]); httpError != nil {
		return httpError
	}
	fmt.Fprint(w, &createTokenResponse{newToken.Claims["aud"].(string), newTokenString})
	return nil
}
