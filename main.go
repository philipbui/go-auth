// auth project auth.go
package main

import (
	"auth/config"
	"auth/controller"
	"auth/route"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Welcome!\n")
}

func Hello(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	fmt.Fprintf(w, "hello, %s!\n", ps.ByName("name"))
}

func main() {
	router := httprouter.New()

	router.GET("/", Index)
	router.GET("/token", route.ServeHTTP(controller.ValidateToken))
	router.POST("/token", route.ServeHTTP(controller.CreateToken))
	router.DELETE("/token", route.ServeHTTP(controller.DeleteToken))
	router.PATCH("/token", route.ServeHTTP(controller.RefreshToken))
	log.Fatal(http.ListenAndServe(config.Port, router))
}
