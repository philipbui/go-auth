package service

import (
	"auth/config"
	"auth/route"
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
)

func CreateJwtToken(userId string) *jwt.Token {
	token := jwt.New(jwt.SigningMethodHS256)
	token.Claims["aud"] = userId
	token.Claims["jti"] = time.Now().Unix()
	return token
}

func SignToken(token *jwt.Token) (string, *route.HttpError) {
	tokenString, err := token.SignedString([]byte(config.Key))
	if err != nil {
		return "", &route.HttpError{err, "Unable to generate token", 500}
	}
	return tokenString, nil
}

func ParseToken(r *http.Request) (*jwt.Token, *route.HttpError) {
	authorization := r.Header.Get("Authorization")
	token, err := jwt.Parse(authorization, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signature: %v", token.Header["alg"])
		}
		return config.Key, nil
	})
	if err != nil {
		return nil, &route.HttpError{err, err.Error(), 401}
	} else if !token.Valid {
		return nil, &route.HttpError{nil, "Invalid token", 401}
	}
	return token, nil

}
