package service

import (
	"auth/config"
	"auth/route"
	"log"
	"time"

	"github.com/garyburd/redigo/redis"
)

var Pool *redis.Pool

func newPool(server, password string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", server)
			if err != nil {
				log.Fatal("duma")
				return nil, err
			}
			/*if _, err := c.Do("AUTH", password); err != nil {
				c.Close()
				return nil, err
			}*/
			return c, err
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
}

func init() {
	Pool = newPool(config.RedisServer, config.RedisPassword)
}
func CreateToken(conn redis.Conn, key, value interface{}) *route.HttpError {
	if reply, err := conn.Do("SADD", key, value); err != nil {
		return &route.HttpError{err, "Unable to save token", 500}
	} else if reply == 0 {
		return &route.HttpError{err, "Token already exists", 401}
	}
	return nil
}

func ValidateToken(conn redis.Conn, key, value interface{}) *route.HttpError {
	conn.Do("SISMEMBER", key, value)
	if reply, err := conn.Receive(); err != nil {
		return &route.HttpError{err, "Unable to get token", 500}
	} else if reply == 0 {
		return &route.HttpError{nil, "Token could not be found", 401}
	}
	return nil
}

func DeleteToken(conn redis.Conn, key, value interface{}) *route.HttpError {
	conn.Do("SREM", key, value)
	if reply, err := conn.Receive(); err != nil {
		return &route.HttpError{err, "Unable to delete token", 500}
	} else if reply == 0 {
		return &route.HttpError{nil, "Token could not be found", 404}
	}
	return nil
}
